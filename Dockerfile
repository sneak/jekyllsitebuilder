FROM ruby:3.1-buster as builder

# I sort of hate that this is duped in .drone.yml and here.
# note that if you update one, you probably have to update the other.
RUN apt update && apt install -y \
    bsdmainutils \
    build-essential \
    make \
    bundler \
    ghostscript \
    imagemagick \
    libxml2-dev \
    libxslt-dev \
    pkg-config

RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash - && \
    apt-get install -y nodejs

ADD ./Gemfile* ./

RUN npm install -g npm && \
    npm install -g yarn && \
    echo "gem: --no-ri --no-rdoc" > ~/.gemrc && \
    yes | gem update --system && \
    gem install bundler && \
    bundle install && \
    yarn global add \
    https://github.com/netlify/cli#a3528d2a679807722de40b1dd87200176b121dbd && \
    netlify --telemetry-disable && \
	bundle install
