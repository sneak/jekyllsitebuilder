YYYYMMDD := $(shell date +%Y-%m-%d)

default: build push

build:
	docker build -t git.eeqj.de/sneak/jekyllsitebuilder:$(YYYYMMDD) .

push:
	docker login git.eeqj.de
	docker push git.eeqj.de/sneak/jekyllsitebuilder:$(YYYYMMDD)
